from decimal import Decimal
from functools import reduce
import pandas as pd 

class FilmsDataStatsGenerator(object):

    def __init__(self, films_api_service):
        self.films = pd.DataFrame(films_api_service())
        self.films.releaseDate = pd.to_datetime(self.films.releaseDate,format="%Y/%m/%d")

    def get_best_rated_film(self, director_name) -> str:
        """TODO Implement
        Retrieves the name of the best rated film that was directed by the director with the given name.
        If there are no films directed the the given director, return the None object.
        Note there will only be one film with the best rating.
        """
        director_films = self.films[self.films.directorName == director_name]

        if len(director_films):
            film = director_films.iloc[director_films.rating.argmax()].to_dict()
            return film['name']

        return None 

    def get_director_with_most_films(self) -> str:
        """TODO Implement
        Retrieves the name of the director who has directed the most films in the CodeScreen Film service.
        """

        director_most_films = self.films['directorName'].mode()[0]
        return director_most_films

    def get_average_rating(self, director_name) -> Decimal:
        """TODO Implement
        Retrieves the average rating for the films directed by the given director, rounded to 1 decimal place.
        If there are no films directed the the given director, return the None object.
        """
        director_films = self.films[self.films.directorName == director_name]

        if len(director_films):
            average = director_films.rating.mean()
            return round(average,1)

        return None

    def get_shortest_number_of_days_between_film_releases(self, director_name) -> int:
        """TODO Implement
        Retrieves the shortest number of days between any two film releases directed by the given director.
        If there are no films directed by the given director, return the None object.
        If there is only one film directed by the given director, return 0.
        Note that no director released more than one film on any given day.

        For example, if the service returns the following 4 films,

        {
            "name": "Batman Begins",
            "length": 140,
            "rating": 8.2,
            "releaseDate": "2006-06-16",
            "directorName": "Christopher Nolan"
        },
        {
            "name": "Interstellar",
            "length": 169,
            "rating": 8.6,
            "releaseDate": "2014-11-07",
            "directorName": "Christopher Nolan"
        },
        {
            "name": "Prestige",
            "length": 130,
            "rating": 8.5,
            "releaseDate": "2006-11-10",
            "directorName": "Christopher Nolan"
        },
        {
            "name": "Black Hawk Down",
            "length": 152,
            "rating": 7.7,
            "releaseDate": "2001-1-18",
            "directorName": "Ridley Scott"
        }

        then this method should return 147 for Christopher Nolan, as Prestige was released 147 days after Batman Begins.
        """
        director_films = self.films[self.films.directorName == director_name]

        if len(director_films):
            if len(director_films) == 1:
                return 0
            director_films = director_films.sort_values(by='releaseDate')
            dates = director_films.shift(-1).releaseDate - director_films.releaseDate
            return min(dates).days

        return None

if __name__ == "__main__":

    from films_api_service import FilmsAPIService

    _generator = FilmsDataStatsGenerator(FilmsAPIService())
    print(_generator.get_best_rated_film('Christopher Nolan'))
    print(_generator.get_director_with_most_films())
    print(_generator.get_average_rating('Christopher Nolan'))
    print(_generator.get_shortest_number_of_days_between_film_releases('Christopher Nolan'))