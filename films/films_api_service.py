import requests
from .constants import *

class FilmsAPIService(object):

    # Your API token. Needed to successfully authenticate when calling the films endpoint. 
    # This needs to be included in the Authorization header in the request you send to the films endpoint.
    @staticmethod
    def get_all_films()->list:
        """
        Retrieves the data for all films by calling the https://app.codescreen.dev/api/assessments/films endpoint.
        """
        headers = {
            "Authorization": f"Bearer {API_SUPER_SECRET_TOKEN}",
            "Content-type": "application/json"
        }

        return requests.get(FILMS_ENPOINT_URL, headers=headers).json()
    
    def __call__(self,):
        return FilmsAPIService.get_all_films()

    def __repr__(self,):
        return FilmsAPIService.get_all_films()

    @property
    def endpoint(self,):
        return FILMS_ENPOINT_URL

if __name__ == "__main__":
    films = FilmsAPIService.get_all_films()
    print(films)